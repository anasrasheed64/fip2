main() {
  // find the output

  print('Step By Step Academy'.substring(
      5, 15)); // By Step Ac    , By Step Aca   , Step By Ste  , error
  print('Hello Every Body'
      .substring(6)); // Every Body    , Every Bod   , Every Body   , error
  print('Hello Every Body'.length); // 16 , 15 , 17 , 18
  print('Hello Every Body'.startsWith('he'.toLowerCase()));
  print('Hello Every Body'.toUpperCase().endsWith('dy'.toLowerCase()));
  print('Hello Every Body'.toLowerCase().endsWith('dy'.toLowerCase()));
  print('Hello Every Body'.isEmpty);
  print('Hello Every Body'.isNotEmpty);
  print('Hello Every Body'.indexOf('e'));
  print('Hello Every Body'.indexOf(' '));
  print('Hello Every Body'.substring(2).indexOf('e'));
  print('Hello Every Body'.substring(2).indexOf('E'));
  print('Hello Every Body'.substring(5).indexOf('E'));
  print('Hello Every Body'.lastIndexOf('e'));
  print('Hello Every Body'.substring(1).lastIndexOf('H'));

  print('Hello Every Body'.contains('every'));
  print('Hello Every Body'.contains('ody'));
  print('Hello Every Body'.contains('hello'));
  print('Hello Every Body'.contains('ello'));
  print('Hello Every Body'[6] + 'nas is the best');
  print('Hello Every Body'[6] + 'nas is the best'.indexOf('e').toString());
  print(5 > 5);
  print(5 != 5);
  print(10 * 5 > 5 * 5 + 25 * 1);
  print(-1 * (55 / 5 - 5) * -2);
  print(2 * 8 / 4 <= 9 * 4 / 6);
  print(2 * 8 / 4 <= 9 * 4 / 6);

  int number = -25;
  number++;
  int number2 = number;
  number2 *= 4;
  print(number.toString() + '15');

  int number3 = -22;
  number3++;
  int number4 = number3;
  number4 *= 4;
  print(number3 - 50);

  // int x = 10 / 5;
  // print(x);

  // string s = 's';
  // print(s.toUpperCase());

  for (int index = 0; index > 0; index++) {
    print(index.toString() + ' DART');
  }

  for (int index = 0; index <= 0; index++) {
    print(index.toString() + ' Flutter');
  }

  for (int index = 10; index >= 5; index--) {
    print(index.toString() + ' Android');
    index--;
  }

  String ali = "ali";

  switch (ali) {
    case 'Ahmad':
      print('Ali');
      break;
    case 'Ali':
      print('Ahmad');
      break;
    case ' ali ':
      print('Ali is the best');
      break;
    default:
      print('Ali isDefault');
  }

  int banana = 100;
  if (banana >= 50)
    print('the price is greater than 50');
  if (banana >= 100)
    print('the price is greater than 100');
  else
    print('banana is less than 100');

  // write a program code to print  the last 5 characters in word ( International )

  // write a program code to print the largest number between these 3 numbers (10 , 20 , 30) Note : use If Condition
}
