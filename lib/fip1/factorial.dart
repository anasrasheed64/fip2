main() {
  // 5!  5*4*3*2*1  = 120
  // 7! 7*6*5*4*3*2*1
  int number = 5;
  print(findFactorial(number));
}

int findFactorial(int number) {
  int result = 1;
  while (number >= 1) {
    result = result * number;
    number--;
  }
  return result;
}
