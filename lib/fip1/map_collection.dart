main() {
  // [ 1, 2, 3 ,4 5, 6 ] index  list
  // { key : value  } { 'one': 1 , 'two': 2  , 'three' : 3 } map.get('one'); // 1  map.get('three);  // 3
  Map<String, int> map = {'One': 1, 'Two': 2, 'Three': 3, 'Four': 4};
  // map.update('Three', (value) => map['Four'] / map['Two']);
  print(map.length); // 2
  print(map.isEmpty); // false
  print(map.isNotEmpty);
  print(map.remove('Two'));
  print(map.length);
  print(map.update('One', (value) => 5));
  print(map.containsKey('Three')); // bool false
  print(map.containsKey(1)); // bool false
  print(map['Four']);
  //print(list[0])

  // map.clear();
  // print(map.isEmpty); // true

  Map<String, double> myMap = {'One': 1, 'Two': 2, 'Three': 3, 'Four': 4};
  myMap.update('Three', (value) => myMap['Four'] / myMap['Two']);

  List<double> values = myMap.values.toList();
  List<String> keys = myMap.keys.toList();
  print(myMap.keys.toList().toString());
  print('the map values is ' + values.toString());
  print('the map keys is ${keys.toString()}');
  Map<String, dynamic> jsonMap = {
    'Anas': 'anas',
    'Ahmad': 5,
    'Mais': 10.5,
    'Ali': true,
    'Abd': 'Abed',
    'ammar': false
  };

  print(jsonMap['Mais']);
  print(jsonMap['ammar']);
  print(jsonMap['Ahmad']);
  print(jsonMap['Ali']);
  print(jsonMap['Abd']);
}
