main() {
  Eagle e = Eagle("Eagle", 30, 'as');
  e.vision();
  e.speak();
  e.fly();
  // e.canFly();
  Parrot parrot = new Parrot(name: 'sami', t: 'af', age: 10);
  print(parrot.name);
  print(parrot.age);
  parrot.fly();
  parrot.speak();
  Bird bird = new Bird('asd', 30);
  Eagle eagle = new Eagle('ahmad', 35, 'sdf');
  eagle.name;
  eagle.age;
  eagle.fly();
  eagle.speak();
  eagle.vision();
}

abstract class Animal {
  bool canFly(); // without implementation / body
  String name;
  int age;

  Animal(this.name, this.age);
}

class Bird {
  String name;
  int age;

  Bird(this.name, this.age); // : super(name, age);
  // Bird({String name, int age}){
  //   this.name=name;
  //   this.age=age;
  // }
  void fly() {
    print("The bird can fly");
  }

// @override
// bool canFly() {
//   return true;
// }
}

class Parrot extends Bird /* super class*/ {
  // inheritance
  String t;

  Parrot({String name, int age, this.t}) : super(name, age);

  void speak() {
    print("The parrot can speak");
  }
}

class Eagle extends Parrot {
  Eagle(String name, int age, String t)
      : super(
          name: name,
          age: age,
          t: t,
        );

  void vision() {
    print("The eagle has a sharp vision");
  }
}
