main() {
  Rectangle rec = Rectangle(10, 20);
  Circle circle = new Circle(7.5);
  printArea(rec); // dependency injection
  printArea(circle);
  Shape rec3=new Rectangle(20, 10); // polymorphism

  // handleCircle(circle); // calling
  // handleRectangle(rec);
  // Shape x = new Rectangle(10, 5); // polymorphism
  // Shape y = new Circle(7.5); // polymorphism

  // super class  , name of the variable = new any of subClass
}

// void handleCircle(Circle c) {
//   print(c.getArea());
// }
//
// void handleRectangle(Rectangle rec) {
//   print(rec.getArea());
// }

void printArea(Shape shape) {
  print(shape.getArea());
}

abstract class Shape {
  double getArea();
  double mohet();
}

class Rectangle implements Shape {
  double height;
  double width;

  Rectangle(this.height, this.width);

  @override
  double getArea() {
    return height * width;
  }

  @override
  double mohet() {
    return height * 2 + width * 2;
  }
}

class Circle extends Shape {
  double radius;

  Circle(this.radius);

  @override
  double getArea() {
    return radius * 2;
  }

  @override
  double mohet() {
    return radius + radius * 3;
  }
}
