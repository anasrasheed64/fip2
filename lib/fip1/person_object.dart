class Person {
  int age;
  String name;
  double height;
  double weight;
  String gender;
  String ssn;
  String dateOfBirth;
  String _major; // private _   String major='dfsdf'   String _major='werwer';

  Person(String name, int age, String gender, String major,
      {this.height, // optional parameter
      this.weight,
      this.ssn,
      this.dateOfBirth}) {
    // constructor  the name of constructor must be the same name of the class
    this.name = name;
    this.age = age;
    // this.height = height;
    // this.weight = weight;
    this.gender = gender;
    // this.ssn = ssn;
    // this.dateOfBirth = dateOfBirth;
    this._major = major;
  }

  String get major {
    return _major;
  }

  set major(String val) {
    _major = val;
  }

  String getMajor() {
    return _major;
  }

  void setMajor(String val) {
    this._major = val;
  }

// String get major { // getter
//   return _major;
// }

// ctrl + prt   ,, right click generate setter , ,,, fn + alt +prt

// Person();

// constructor default values

} // class name always start with Capital

main() {
  String ahmad = ''; // decleration  null
  print(ahmad); // null pointer exception
  Person mohannad = new Person('Mohannad', 20, 'male', 'CS',
      ssn: '9981014775',
      dateOfBirth: '1980',
      weight: 100,
      height: 180); // create new object
  // mohannad.name = 'Anas2'; // init
  // mohannad.age = 30;
  // mohannad.height = 180;
  // mohannad.weight = 80;
  // mohannad.gender = 'Male';
  // mohannad.ssn = '9911014557';
  // mohannad.dateOfBirth = '04/08/1980';
  print(mohannad.name);
  print(mohannad.age);
  print(mohannad.ssn);
  print(mohannad.gender);
  print(mohannad.height);
  print(mohannad.weight);
  print(mohannad.dateOfBirth);
  print(mohannad._major);

  Person ali = Person(
    'Ali',
    28,
    'Male',
    'CIS',
  );
  List<String> a = ["Ali", "Ahmad", "Mais"];
  List<String> b = [];
  List<String> c = a.toList(); // clone // temp list  for
  print(a);
  print(c);
  a.add("Anas");
  print(a);
  print(c);
  //  int x; // decleration null
  // if(x==null) // validation
  //   x=5;
  // print(x);
}
