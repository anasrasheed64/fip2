import 'package:fip2/fip2/classes.dart';

main() {
  List<int> myList = [10, 20, 30, 5, 40, 8, 45, 82, 0, 15];
  print(findMax(myList).toString() + "is the max "); // calling function/method
  List<int> myList2 = [10, 20, 30, 5, 40, 8, 45, 20, 0, 15];
  print(findMax(myList2).toString() + "is the max2 ");
}
// return type  methid name ( arguments , parameters  ) {}                                 we use lowerCamelCase

int findMax(List<int> list) {
  int max = list[0]; // assume the max is the first element
  for (int item in list) {
    if (item > max) max = item;
  }
  return max;
  max = 20; // DeadCode
  return max;
}

void p(/* Non Argument*/) {
  print("P");
}

void calc(int num) {
  if (num > 500) return;
  if (num > 0) {
    print('positive');
  } else if (num < 0) {
    print('negative');
  } else {
    print('0');
  }
}

int max(int a, int b, int c) {
  int max = a;
  if (b > a && b > c)
    max = b;
  else if (c > a && c > b) max = c;

  return max;
}

class C {
  void calling() {
    // Person p = Person("anas", 1, "123", DateTime.now());
    Person p = Person();
    p.setName('Ali');
    p.id = "asdasd";
    print(p.getName()); // 'sami'
    print(p.age); // 1
    // p.name = 'anas'; // setter dart
    // p.setName("anas");
  }
}
