main() {
  List list = []; // O(n/2)
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asdads");
  list.add("asda"); // index

  // {   "anas" : 5 , "Ahmad" : 12}

  Map<String, int> map = {
    "anas": 10,
    "ahmad": 15,
    "Sami": 14,
  }; //  { } O(1)
  Map<String, int> map2 = {};

  map.putIfAbsent(
      "anas",
      () =>
          12); // Returns the value associated to [key], if there is one. if not exist will add it

  map["anas"] = 12;
  print(map["anas"]);
  map.addAll(map2);
  List<String> keys = map.keys.toList();
  List<int> values = map.values.toList();
  print(keys);
  print(values);

  //   { 123:"anas"  }


  // java 8


}
