import 'package:fip2/fip2/oop2.dart';

class Shape { // super class
  // naming convension
  int numofAdla3;
  double area;
  double mo7ee6;

  Shape(this.numofAdla3, this.area, this.mo7ee6);
}

class Circle extends Shape { // child - subclass
  double radius;

  Circle(int numofAdla3, double area, double mo7ee6, double radius)
      : super(numofAdla3, area, mo7ee6) {
    this.radius = radius;

    double calcArea() {
      return area * mo7ee6 / 2;
    }


  }

  @override
  String toString() {
    return 'Circle{radius: $radius}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Circle &&
          this.radius == other.radius;

  @override
  int get hashCode => radius.hashCode;
}

abstract class A {
  // abstract and non abstract methods
  double area(double val); // abstract method
  double hello(double val); // abstract method
  double mo7e6() {
    // non abstract method
    return 10;
  }
}

class B extends A {
  @override // new impl
  double area(double val) {
    return val*2;
  }

  @override
  double hello(double val) {
    // TODO: implement hello
    throw UnimplementedError();
  }
}

// keyword
class Main {
  mainMethod() {
    Shape shape = Shape(4, 10, 12);
    Circle circle = Circle(1, 5, 6, 8);
    circle.radius;
    Square square;
    Circle circle2 = Circle(1, 5, 6, 8);
    print(circle); // // @A125Hjo89sd
    print(circle);
    print(circle==circle2); // true   check by value using override operator
    print(circle==circle2); // true
  }
}
//my name is motasem