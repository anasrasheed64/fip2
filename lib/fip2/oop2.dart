import 'package:fip2/fip1/oop_abstract.dart';

abstract class MyShape {
  int numOfAdla3;

  MyShape(this.numOfAdla3);

  double area();

  double mo7e6();
}

class Square extends MyShape {
  // todo clean code
  Square(int numOfAdla3) : super(numOfAdla3);
 // todo clean hamza
  @override
  double area() {
    return numOfAdla3 * 4 as double;
  }

  @override
  double mo7e6() {
    // this is to calc mo7e6
    return numOfAdla3 * 2.0 + 5;
  }
}

class Rectangle extends MyShape {
  Rectangle(int numOfAdla3) : super(numOfAdla3);

  @override
  double area() {
    return numOfAdla3 * 8 as double;
  }

  @override
  double mo7e6() {
    return area() * 8;
  }

}

class Main {
  main() {
    Square square = Square(4);
    Rectangle rec = Rectangle(4);
    MyShape rec2=new Rectangle(8);   // Heap  new Object
    MyShape rec3=new Rectangle(8);   // Heap  new Object
    print(square.area());
    print(rec.area());
    printMo7e6AndSquareForAllShapes(rec);
    printMo7e6AndSquareForAllShapes(square);
  }

 void printMo7e6AndSquareForAllShapes(MyShape shape){ // dependency injection
   print(
       " the mo7e6 of shape is ${shape.mo7e6()} and the area is ${shape.area()}");
  }
  
}
