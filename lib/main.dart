
import 'package:fip2/fip1/person_object.dart';

import 'fip1/person_object.dart';

main() {
  // int 4 byte   // double 8 bytes  // boolean 1 bit // string
  print('hello this is our first line in dart language :) ');
  String anas = "My Name is Anas";
  print(anas); // My Name is Anas
  String anasFamily =
      'Rasheed'; // $  _ // not started with a number   , space not allow, keywords not acceptable ,  ($  _ ) ALLOW ,
  String anas_family =
      'Rasheed'; //   small camel case  (method , variable ) anasFamily    int firstIndex=0;   int x; int y;
  int firstIndex = 0;
  print(firstIndex);
  String world = 'World';
  print('hello ' + world); // concatenation   hello World
  print('hello $anasFamily');
  print('Hello ' + anasFamily);
  print('Hello $anasFamily');
  print(anasFamily);
  int number = 5; //
  print('number is $number'); // number id 5
  print('number is ' + number.toString()); // number id 5
  int number2 = 10;
  print('number2 is $number2');

  // 10  + 5  is
  print('$number2  + $number is ${number2 + number}');
  print('${number2 + number}    ----   $number2 + $number');
  print('$number2 * $number is ${number2 * number}');
  print('$number2 - $number is ${number2 - number}');
  print('$number2 / $number is ${number2 / number}');

  print(anasFamily); // Rasheed
  print('My Family is ' + anasFamily); // my family is anasRasheed
  print('My Family is $anasFamily');
  int num22 = 22;
  int num33 = 33;
  print('22 + 33 =' + (num22 + num33).toString());

  double realNumber = 22.5;
  print('real number is $realNumber');
  print('$realNumber + $number2 is ${realNumber + number2}');
  print('$realNumber / $number2 is ${realNumber / number2}');

  String s = 'sAeDtH'; // Class   methods    function
  print(s.toUpperCase()); //// string method  // S
  print(s.toLowerCase()); // saedth
  var x2;
  x2 = "yyy";
  x2 = 25;
  x2 = 14.15;
  // dont use var    4 byte      1 bit
  var x = 4;
  var y = 'Anas';
  var d = 77.15;

  num genericNumber = 55; // 8 byte   4 byte
  num genericNumber2 = 22.14; // 8 byte  8 byte
  print('generic number ' + genericNumber.toString());
  print('generic number $genericNumber');
  print(
      '$genericNumber2 + $genericNumber is ${genericNumber2 + genericNumber}'); // 77.14

  print('$anas length is ${anas.length}'); // my name is anas length is 15
  print('$anas to uppercase is ${anas.toUpperCase()}');
  print('$anas to lowercase is ${anas.toLowerCase()}');
  print(anas.trim()); // '     abcd     '  ábcd
  print(
      '$anas substring ${anas.substring(0, 3)}'); // my name is anas     substring    dart 0 base   last index = length-1  first index=0
  // anas  0,2  an 0,1 , [0,3) , anas.substring(3,11)      name is _
  print('is $anas Empty Value ? ' + anas.isEmpty.toString());
  print('ahmad'.substring(1));

  // hello world  substring (1,5)  -->   ello    substring(1)  ello world
  String A = "";
  print(A.length); // 0
  print(A.isEmpty); // true
  print(A.isNotEmpty); // false
  bool v = true; //
  bool v2 = false; // true false
  String B = 'Anas';
  // String methods
  print('is contains an ${anas.contains('an')}'); // my name is anas     true
  print('is contains ww ${anas.contains('ww')}'); // false
  print('is contains as ${anas.contains('as')}'); // true
  print('is ens with an ${anas.endsWith('an')}'); // false
  print('is ens with an ${anas.endsWith('as')}'); // true
  print('is ens with an ${anas.endsWith('nas')}'); // true
  print('is ens with an ${anas.endsWith('  anas')}'); // false
  print('is ens with an ${anas.endsWith(' anas')}');
  print('is ens with an ${anas.startsWith('anas')}');
  // true
  print('**********************************');
  // print(anas.split('').toString());
  print('**********************************');
  print(anas[0]); // index // my name is anas   --> m
  print(anas[11]); // index // my name is anas   --> a
  print('**********************************');
  print(anas.indexOf('s')); // my name is anas  --> 9 left to right
  print('**********************************');
  print(anas.lastIndexOf('a')); // right to left 13
  print(anas.lastIndexOf('w')); // right to left -1
  print(anas.indexOf('w')); // right to left -1
  print('**********************************');
  print(anas.isNotEmpty); // true
  print('**********************************');
  print(anas.startsWith('My')); // case sensetive   asci code    ---> false
  print('**********************************');
  print(anas.startsWith('my')); // ---> true
  print('**********************************');
  print('anas' == 'Anas'); //  ==    --> false
  print('**********************************');
  print(
      'AnAs'.toUpperCase() == 'aNaS'.toUpperCase()); // ANAS == ANAS  ---> true
  print('**********************************');
  print('anas' == 'anas '.trim()); // true
  print('**********************************');
  String sami = 'Sami';
  print('**********************************');
  String ahmad = 'Ahmad';
  print('**********************************');
  print('sami == ahmad ? ' + sami == ahmad); // false
  print('**********************************');
  sami =
      ahmad; // same = Same  ahmad = Ahmad   //////   same = Ahmad ahmad Ahmad
  print('**********************************');
  print(ahmad == sami);
  print('**********************************');
  print('**********************************');
  print('**********************************');
  print('**********************************');

  print(5 > 3); // < > == <= >=   true false   ---> true
  print(22 <= 8); // false
  print(5 <= 2); // false
  print(8 <= 10); // true
  print(5 == 5); // true
  print(3 == 3); // true
  print(3 == 3 ||
      5 == 8); // ||    &&      || OR    && AND      true or false = true ;
  //   true || false   = true

  /*
  true || false = true
  true || true = true
  false || false= false


  if true is exist the the answer is always true

  true && true = true
  true && false = false
  false && false = false

  if false is exist then the answer is always false
  *
  * */
  print(3 == 3 && 5 == 8);
  // true && false = false
  print(5 * 2 ==
      9 * (1 + 1)); // priority ()   */  +-  (== , > , < , >= , <= , !=)
  // 5 * 2 == 9 * 2
  // 10 == 9*2
  // 10 == 18 = false
  print(5 * 2 == 9 * 1 + 1);
  // 10 == 9*1+1
  // 10 == 9+1
  // 10 == 10 ---> true
  print('anas' == 'an' + 'as'); // 'anas' == 'anas'  ---> true

  if ('anas' == 'anas2') //   if (// expression)  شرط مقارنة
    print('anas');

  if ('anas' == 'anas2') //   if (// expression)  شرط مقارنة
    print('anas'); // NOO
  else if ('anas ' == 'anas3') print('ttttt');

  if ('anas' == 'anas2') //   if (// expression)  شرط مقارنة
    print('anas'); // NOO
  else if ('anas' == 'anas3')
    print('ttttt');
  else if ('anas' == 'anas4')
    print('ttttt');
  else if ('anas' == 'anas4')
    print('ttttt');
  else
    print('not anas');

  int number5 = 5; //
  // < > == <= >=  !=
  // switch (variable) {
  // case 0 : print(0); break;     if(variable ==0 )
  // case 1 :  print(1) break;                   if(variable ==1 )

  // }
  switch (number5) {
    case 0:
      print(0);
      break;
    case 1:
      print(1);
      break;
    case 2:
      print(2);
      break;
    case 3:
      print(3);
      break;
    default:
      print(55); // ----> output
  }
  int r2 = 5;
  r2 = 4;
  r2 = 8;
  r2 = 25;

  int k = 35;

  k = r2; // k=25

  print(k); // 25
  r2 = 6; // r=6
  k = r2; // 6
  print(k); // 6
  print(r2); // 6

  int w = 1;
  w = w + 1; // w = 1 + 1 = 2   w++;   w--;
  w = w + 4; // 6
  w = w * 2; // 12
  w = w - 4; // 8
  // w=w/2; // double num  (division) 5/1 = 5.00  not allowed with integer variable 4.00
  // Casting  convert type to type   (int to double   , double to int , string to int , int to string )
  // w++ increase one to variable w
  // w-- decrease one to variable w
  // w+=2;   w=w+2;
  // w*=3;  w=w*3;
  // w-=25;  w = w - 25 ;
  int number6 = 50;

  number6++; // 51
  number6--; // 50
  number6 -= 2; // 48
  number6 += 2; // 50
  number6 *= 3; // 150  number6=number6*3;
  // here      note  substring start index only , modulation  , int methods

  int batata = 5;
  int tomato = 6;
  print(batata != tomato); // == !=

  print(10 / 5); // 2  2.00
  print(10 % 5); // mod  10/5 = 2 --> 0
  print(11 % 5); // mod  11/5 = 2 --> 1

  print(batata.isOdd); // true
  print(batata.isEven); // false
  print(tomato.isEven); // true

  print('my name');
  print('my name');
  print('my name');
  print('my name');
  print('my name');
  print('my name');

  // for (// start index ; condition , increasing,decreasing optional){
  //
  // // }                  counter

  for (int counter = 0; counter < 5; counter++) {
    // counter = 0  0<5 true  counter = 1 counter =2 counter 5
    print('Hello');
  }

  for (int index = 3; index > 0; index--) {
    // index =3 true 3>0  index =2; index 1 index 0
    print('have a nice weekend');
  }
  // have a nice weekend
  // have a nice weekend
  // have a nice weekend

  for (int index = 0; index > 1; index++) {
    // index 0
    print('hello');
  }

  for (int x = 0; x <= 10; x += 2) {
    // index 0 , 0 < 10 true x=2 x=4 x=6 x=8  x=10 x=12
    print('hello');
  }
  // hello
  // hello
  // hello
  // hello
  // hello
  // hello

  for (int x = 0; x <= 10;) {
    // x=0 x=1 x=3 x=4 x=6 x=7 x=9 x=10  x=12
    print('hello');
    // x++;
  }

  for (int x = 10; x > 0; x++) {
    print('hello');
  }

  for (int counter = 0; counter < 10; counter += 2) {
    // counter 0  counter = 2 counter =4 counter =6 counter =8  counter =10
    if (counter == 2) print('the number is 2');
    if (counter == 4) print('the number is 4');
    if (counter == 6) continue; // ما بتنفذ الي بعدييييييييييها
    print(counter);
  }
  // 0
  // the number is 2
  // 2
  // the number is 4
  //  4
  //  8
  for (int counter = 0; counter < 10; counter += 2) {
    // counter 0  counter = 2 counter =4 counter =6 counter =8  counter =10
    if (counter == 2) print('the number is 2');
    if (counter == 4) print('the number is 4');
    if (counter == 6) break; // بتطلع برا الفور
    print(counter);
  }

  // if(5>3)
  //   print('>');
  // else if (5>4)
  //   print('<');
  // >

  // if(5>3)
  //   print('>');
  // if(5>4)
  //   print('<');
  // > <
  // infinite loop
  // hello
  // hello
  // hello
  // hello

  // while (condition){
  // }
  int r = 5;
  while (r > 1) {
    // r=4 r=3
    print('see you');
    if (r == 3) break;
    r--;
  }

  // see you
  // see you
  // see you

  // r=3
  while (r > 1) {
    // r=4 r=3
    print('see you');
    if (r == 3) continue;
    r--;
  } // infinite loop

  r2 = 8;
  do {
    print('Ali');
    r2--;
  } while (r2 > 5);

  // if else if switch

  // Ali r2=7

  Person person = new Person('Mohannad', 20, 'male', 'CS', // new object
      ssn: '9981014775',
      dateOfBirth: '1980',
      weight: 100,
      height: 180);
  person.name = 'Abed';
  print(person.name);
  print(person.major);
  person.major = 'SE';
  print(person.major);
  print(person.getMajor());
  person.setMajor('CIS');
  print(person.major);

  // inheritance
}
